# Weather App Project

This is a weather app built with React Native and powered by OpenWeather API and Google Places API.

# Stack

React Native, OpenWeather API, Google Places Autocomplete API

# Installation

You need to have Expo installed on both your PC/laptop and on your mobile. This project was built using an Android phone.

Expo installation: `npm install expo-cli --global` 

Clone the repo: `git clone https://gitlab.com/joyce4/weather_app.git`

Change to the project folder: `cd weatherappbcsproj`

Add dependencies: `npm install`

# Running the App

To run the app, from your terminal and changing to the project folder: `expo start`

Scan the QR code from the Expo app.
