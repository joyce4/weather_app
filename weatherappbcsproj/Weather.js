import React, {Component} from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { Divider, Image } from 'react-native-elements';
import Constants from 'expo-constants';
import WeekdayNames from './WeekdayNames';
import WeatherIcons from './WeatherIcons';
 
export default class Weather extends Component {

  state = {
    message: '',
    list: this.props.forecast
  }

  render () {

    // will leave out the first part only 
    let list = this.props.forecast;
    //let sixDays = list.slice(1, 7);
    let threeDays1 = list.slice(1, 4);
    let threeDays2 = list.slice(4, 7);

    return (
      <View style={{flex: 5}}>
         <View style={styles.forecastContainer}>
          {threeDays1.map((list, i) => {
            return (
              <View key={i} style={{ width: '30%'}}>
                <WeekdayNames 
                  index={i}
                  ts={list.dt}
                />
                <Text style={{ textAlign: 'center' }}>
                  {list.weather[0].description}
                </Text>
                <View>
                  <WeatherIcons 
                    main={list.weather[0].main}
                  />
                </View>
                <Text style={{ textAlign: 'center' }}>
                  min {list.temp.min} &#8451;
                </Text>
                <Text style={{ textAlign: 'center' }}>
                  max {list.temp.max} &#8451;
                </Text>
              </View>
            );
          })}
        </View>
        <Divider style={{backgroundColor: 'black', margin: 20}} />
        <View style={styles.forecastContainer2}>
          {threeDays2.map((list, i) => {
            return (
              <View key={i} style={{ width: '30%'}}>
                <WeekdayNames 
                  index={i}
                  ts={list.dt}
                />
                <Text style={{ textAlign: 'center' }}>
                  {list.weather[0].description}
                </Text>
                <View>
                  <WeatherIcons 
                    main={list.weather[0].main}
                  />
                </View>
                <Text style={{ textAlign: 'center' }}>
                  min {list.temp.min} &#8451;
                </Text>
                <Text style={{ textAlign: 'center' }}>
                  max {list.temp.max} &#8451;
                </Text>
              </View>
            );
          })}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  forecastContainer: {
    flex: 3,
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '90%',
    flexWrap: 'wrap',
    marginLeft: 20
  },
  forecastContainer2: {
    flex: 4,
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '90%',
    flexWrap: 'wrap',
    marginLeft: 20
  },
});