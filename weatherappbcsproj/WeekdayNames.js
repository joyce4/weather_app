import React, {Component} from 'react';
import { Text, View } from 'react-native';

export default class WeekdayNames extends Component {

  state = {
    weekday: ''
  }

  componentDidMount () {
    this.getDayfromTxt();
  }

  getDayfromTxt = () => {
    let ts = this.props.ts;
    let date = new Date(ts*1000);
    let weekdays = new Array (
      "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
    );
    var day = date.getDay();
    let dayTxt = weekdays[day];
    this.setState({weekday: dayTxt});
  }

  render () {

    return (
      <View>
        <Text style={{textAlign: 'center'}}>
          {this.state.weekday}
        </Text>
      </View>
    );
  }
}
