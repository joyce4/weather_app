import React, {Component} from 'react';
import { StyleSheet, Text, View, TextInput, Button, Modal } from 'react-native';
//import { apiKey } from './keys/k';
import { Ionicons, MaterialIcons } from '@expo/vector-icons';
import Constants from 'expo-constants';
import { Divider } from 'react-native-elements'; 
import Weather from './Weather';

export default class App extends Component {

  state = {
    error: '',
    latitude: 0,
    longitude: 0,
    forecast: [],
    dateNow: '',
    dayNow: '',
    timeNow: '',
    text: '',
    predictions: [],
    isLoading: true,
    modalVisible: false
  }

  componentDidMount () {
    this.handleLoading();
    this.getTimeNow();
    this.getDayNow();
    this.getDateNow();
    this.getLocation();
  }

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  handleLoading = () => {
    setTimeout(()=> {this.setState({isLoading: false})}, 2000);
  }

  getLocation = () => {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        this.setState({latitude: position.coords.latitude, longitude: position.coords.longitude});
        this.getWeather();
    },
       (error) => this.setState({ forecast: error.message }),
        { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
    );
  }

  getWeather = () => {
    let url = 'https://api.openweathermap.org/data/2.5/forecast/daily?lat=' + this.state.latitude + '&lon=' + this.state.longitude + '&units=metric&appid=16909a97489bed275d13dbdea4e01f59&cnt=7';
    fetch(url)
    .then(response => response.json())
      .then(data => {
        this.setState({forecast: [data]});
        //console.log(url);
        //console.log([data]);
    });
  }

  getLocByName = () => {
    this.setState({isLoading: true, modalVisible: false})
    let url = 'https://api.openweathermap.org/data/2.5/forecast/daily?q=' + this.state.text + '&units=metric&appid=16909a97489bed275d13dbdea4e01f59&cnt=7';
    fetch(url)
    .then(response => response.json())
      .then(data => {
        this.setState({forecast: [data], locClicked: false, text: ''});
        console.log(url);
        //console.log([data]);
        this.handleLoading();
    });
  }

  getDateNow = () => {
    const dt = new Date();
    let months = new Array (
      "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
    )
    var month = dt.getMonth();
    let monthNow = months[month];
    var DD = ("0" + dt.getDate()).slice(-2);
    let completeDate = monthNow + " " + DD;
    this.setState({dateNow: completeDate});
  }

  getTimeNow = () => {
    const dt = new Date();
    var hh = ("0" + dt.getHours()).slice(-2);
    var mm = ("0" + dt.getMinutes()).slice(-2);
    var ss = ("0" + dt.getSeconds()).slice(-2);
    var timeString = hh + ":" + mm + ":" + ss;
    this.setState({timeNow: timeString});
  }

  getDayNow = () => {
    const dt = new Date();
    let weekdays = new Array (
      "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
    );
    var day = dt.getDay();
    let dayNow = weekdays[day];
    this.setState({dayNow: dayNow});
  }

  handleChange = (text) => {
    this.setState({text: text});
    let url = 'https://maps.googleapis.com/maps/api/place/autocomplete/json?input=' + this.state.text + '&types=(cities)&language=en&key=AIzaSyClwSEjBUpjBX11uT8uMyBnJrWERIIa-_g'
    fetch(url)
    .then(response => response.json())
      .then(data => {
        this.setState({predictions: data.predictions});
        //console.log(this.state.predictions);
    });
  }

  handleAutoComplete = text => {
    this.setState({text: text});
  }

  render () {

    let icon;
    let weather;
    if (this.state.forecast.length === 0) {
      return null;
    } else {
      weather = this.state.forecast[0].list[0].weather[0].main;
      if (weather === "Clouds") {
        icon = "md-cloudy";
      }
      if (weather === "Rain") {
        icon = "md-rainy";
      }
      if (weather === "Thunderstorm") {
        icon = "md-thunderstorm";
      }
      if (weather === "Clear") {
        icon = "md-sunny";
      }
      if (weather === "Snow") {
        icon = "md-snow";
      }
    }

    return (
      <View style={styles.container}>
      <View>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
          }}>
          <View style={{flex: 1, backgroundColor: 'rgba(0,0,0,0.8)', justifyContent: 'center'}}>
            <Text style={{textAlign: 'center', marginTop: 10, fontSize: 35, color: '#fff'}}>Enter city name</Text>
              <TextInput 
                onChangeText={this.handleChange}
                value={this.state.text}
                style={styles.input}
                name="text"
                onSubmitEditing={this.getLocByName}
                placeholder='Search'
              />
              <View style={{marginTop: '5%'}}>
              {this.state.predictions.map((obj, i) => {
                return (
                  <View key={i} style={{width: '50%', marginLeft: '25%', marginRight: '25%'}}>
                    <Button title={obj.description} onPress={() => this.handleAutoComplete(obj.description)} color="rgba(0,0,0,0.8)"/>
                  </View>
                ) 
              })}
              </View>
              <View style={{width: '50%', marginLeft: '25%', marginRight: '25%'}}>
                <Button style={{textAlign: 'center', fontSize: 20}} onPress={()=>this.setModalVisible(false)} title='Cancel'/>
              </View>
            </View>
         </Modal>
      </View>
      {this.state.isLoading || this.state.forecast.length === 0 ? 
        <View style={styles.loading}>
          <Text style={{textAlign: 'center', fontWeight: '700', fontSize: 30, marginTop: '60%'}}>Getting the weather...</Text>
        </View>
        :
        <View style={styles.container}>
          <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
            <MaterialIcons name="location-on" size={30} onPress={()=>this.setModalVisible(true)}/>
            <Text style={styles.paragraph}>{this.state.dateNow} / {this.state.dayNow}</Text>
            <Text style={styles.paragraph}>
              {this.state.timeNow}
            </Text>
          </View>
          <Text style={{ textAlign: 'center', fontWeight: '700', fontSize: 30, marginTop: 20 }}>
            {this.state.forecast[0].city.name}, {this.state.forecast[0].city.country}
          </Text>
          <Text style={{textAlign: 'center'}}> 
            Lat: {this.state.forecast[0].city.coord.lat} / Lon: {this.state.forecast[0].city.coord.lon}
          </Text>
          <View style={{ flex: 4 }}>
            <Text
              style={{
                textAlign: 'center',
                fontWeight: '700',
                fontSize: 20,
                margin: 10,
              }}>
              {this.state.forecast[0].list[0].temp.day} &#8451; - actual
            </Text>
            <Ionicons name={icon} size={220} style={{textAlign: 'center'}}/>
          </View>
          <Weather forecast={this.state.forecast[0].list}/>
          </View>}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#00b3a6'
  },
  paragraph: {
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  input: {
    height: 50,
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 2,
    width: '80%',
    textAlign: 'center',
    marginLeft: '10%',
    marginRight: '10%',
    marginTop: 10,
    marginBottom: 10,
    color: '#fff',
    fontSize: 25
  },
  loading: {  
    flex: 1,
    backgroundColor: '#7ddbf3' 
  }
});