import React, {Component} from 'react';
import { Text, View } from 'react-native';
import { Ionicons } from '@expo/vector-icons';

export default class WeatherIcons extends Component {

  render () {

    let weather = this.props.main;
    let icon;
    if (weather === "Clouds") {
      icon = "md-cloudy";
    }
    if (weather === "Rain") {
      icon = "md-rainy";
    }
    if (weather === "Thunderstorm") {
      icon = "md-thunderstorm";
    }
    if (weather === "Clear") {
      icon = "md-sunny";
    }
    if (weather === "Snow") {
      icon = "md-snow";
    }

    return (
      <View>
        <Ionicons name={icon} size={60} style={{textAlign: 'center'}}/>
      </View>
    );
  }
}
